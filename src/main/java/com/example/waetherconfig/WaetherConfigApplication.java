package com.example.waetherconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class WaetherConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(WaetherConfigApplication.class, args);
	}
}
